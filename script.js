$(function() {
    GoogleCalendar.Init();
});

var GoogleCalendar = {
    //PROPERTIES
    "GoogleCalendarID": "",
    "GoogleAPIKey": "",
    "TimeObject": {},

    //METHODS
    "Init": function() {
        this.MediaQueries();
    },
    "GetCalendar": function() {
        const url = "https://www.googleapis.com/calendar/v3/calendars/" + params.CalendarID + "/events?maxResults=2500&singleEvents=true&orderBy=startTime&timeMin="+ params.TimeMin +"&timeMax="+ params.TimeMax +"&key=" + params.ApiKey;

        $.getJSON(url, function(data) {                          
            if("[$Caching$]" == "True") {
                sessionStorage.setItem("GoogleEvents"+_this.pmi+"-"+params.CalendarID, JSON.stringify(data));
            }

            params.Complete(data);
        }).fail(function(result) {
            $("#pmi-"+_this.pmi+" .google-calendar-list input[type='checkbox'][value='"+decodeURIComponent(params.CalendarID)+"']").hide().addClass("error");
            $("#pmi-"+_this.pmi+" .google-calendar-list input[type='checkbox'][value='"+decodeURIComponent(params.CalendarID)+"']").parents("li").addClass("flex-column error").removeClass("flex-items-center").append('<p>This calendar must be made public.</p>');
            console.log("An error has occured fetching the Google Calendar. Please review the error result below: \n"+ result.responseText);
        });
    },
    "MediaQueries": function() {
        switch(this.GetMediaQuery()) {
            case "desktop":
            
            break;
            case "768":
            case "640":
            case "480":
            case "320":

            break;
        }
    },
    "GetMediaQuery": function() {
        return window.getComputedStyle(document.querySelector("body"), ":before").getPropertyValue("content").replace(/"|'/g, "");/*"*/
    }
};
